package com.exa.felipesangas.englishdb.entities;

/**
 * Created by felipesangas on 16/6/18.
 */

public class translations {

    private String enString;
    private String esString;

    public translations(){
    }

    public translations(String enString, String esString) {
        this.enString = enString;
        this.esString = esString;
    }

    public String getEnString() {
        return enString;
    }

    public void setEnString(String enString) {
        this.enString = enString;
    }

    public String getEsString() {
        return esString;
    }

    public void setEsString(String esString) {
        this.esString = esString;
    }
}
