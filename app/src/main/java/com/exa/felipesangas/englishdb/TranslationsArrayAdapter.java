package com.exa.felipesangas.englishdb;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.exa.felipesangas.englishdb.entities.translations;

import java.util.ArrayList;

/**
 * Created by felipesangas on 17/6/18.
 */

class TranslationsArrayAdapter extends ArrayAdapter<translations>{

    public TranslationsArrayAdapter(@NonNull Context context, ArrayList<translations> dataToListView) {
        super(context, R.layout.custom_row ,dataToListView);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater buckysInflater = LayoutInflater.from(getContext());
        View customView = buckysInflater.inflate(R.layout.custom_row, parent, false);

        translations currentTranslation = getItem(position);
        TextView enString = (TextView) customView.findViewById(R.id.enStringList);
        TextView esString = (TextView) customView.findViewById(R.id.esStringList);

        enString.setText(currentTranslation.getEnString());
        esString.setText(currentTranslation.getEsString());

        return customView;
    }

    @Nullable
    @Override
    public translations getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


}
