package com.exa.felipesangas.englishdb.utilities;

/**
 * Created by felipesangas on 16/6/18.
 */

public class utilities {

    //Data translations table constants
    public static final String TABLE_NAME = "translations";
    public static final String ES_TRANSLATIONS = "esString";
    public static final String EN_TRANSLATIONS = "enString";


    public static final String CREATE_ENTITIES_TABLE = "CREATE TABLE translations (enString STRING, esString STRING)";
}
