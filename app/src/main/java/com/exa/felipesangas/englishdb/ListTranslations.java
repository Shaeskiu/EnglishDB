package com.exa.felipesangas.englishdb;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.exa.felipesangas.englishdb.entities.translations;
import com.exa.felipesangas.englishdb.utilities.utilities;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListTranslations extends AppCompatActivity {

    SQLiteConnectionHelper conn;
    ArrayList<translations> list;
    ListView listView;
    TranslationsArrayAdapter adapter;
    String row2Delete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_translations);

        conn = new SQLiteConnectionHelper(getApplicationContext(), "bd_translations", null, 1);

        getData();

        listView = (ListView) findViewById(R.id.TranslationsList);
        adapter = new TranslationsArrayAdapter(this, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                row2Delete = list.get(position).getEnString();

                //Confirmation Dialog
                AlertDialog alertDialog = new AlertDialog.Builder(ListTranslations.this).create();
                alertDialog.setTitle(R.string.delete);
                alertDialog.setMessage("Are you sure?"); //TODO: FIX THIS!!
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteConfirm();
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }



    private void getData(){
        SQLiteDatabase db  = conn.getReadableDatabase();

        translations currentTranslation = null;
        list = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM "+utilities.TABLE_NAME, null);

        while(cursor.moveToNext()){
            currentTranslation = new translations();
            currentTranslation.setEnString(cursor.getString(0));
            currentTranslation.setEsString(cursor.getString(1));

            //Log.i("english",currentTranslation.getEnString());
            //Log.i("spanish",currentTranslation.getEsString());

            list.add(currentTranslation);
        }
    }

    public boolean deleteDatabaseColumn(String row2Delete){
        SQLiteDatabase db = conn.getWritableDatabase();
        boolean succes = false;

        if(db.delete(utilities.TABLE_NAME, utilities.EN_TRANSLATIONS + " = " + '"'+row2Delete+'"',  null) > 0)
            succes = true;

        db.close();
        return succes;
    }

    public void updateListView(){
        // get new modified random data
        getData();
        // update data in our adapter
        adapter.clear();
        adapter.addAll(list);
        // fire the event
        adapter.notifyDataSetChanged();
    }

    public void deleteConfirm(){
        //Log.i("ROW", row2Delete);
        if(deleteDatabaseColumn(row2Delete))
            Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
        updateListView();
    }
}
