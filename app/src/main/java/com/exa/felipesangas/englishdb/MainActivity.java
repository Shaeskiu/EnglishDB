package com.exa.felipesangas.englishdb;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.exa.felipesangas.englishdb.utilities.utilities;

public class MainActivity extends AppCompatActivity {

    EditText enInput, esInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteConnectionHelper conn = new SQLiteConnectionHelper(this, "bd_translations", null, 1);

        //Listeners
        Button buttonNew = (Button) findViewById(R.id.newStringButton);
        Button buttonList = (Button) findViewById(R.id.listButton);


        buttonNew.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                saveNewData(v);
            }
        });

        buttonList.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                goListView(v);
            }
        });
    }

    public void goListView(View v) {
        Intent intent = new Intent(getApplicationContext(), ListTranslations.class);
        startActivity(intent);
    }

    public void saveNewData(View view){
        enInput =  findViewById(R.id.enString);
        esInput =  findViewById(R.id.esString);

        SQLiteConnectionHelper conn = new SQLiteConnectionHelper(this, "bd_translations", null, 1);

        SQLiteDatabase db =  conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(utilities.EN_TRANSLATIONS, enInput.getText().toString());
        values.put(utilities.ES_TRANSLATIONS, esInput.getText().toString());

        Long result = db.insert(utilities.TABLE_NAME, utilities.EN_TRANSLATIONS, values);

        Toast.makeText(getApplicationContext(), "Saved Complete!" + result, Toast.LENGTH_SHORT).show();

        db.close();
    }

}
