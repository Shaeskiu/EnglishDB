package com.exa.felipesangas.englishdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.exa.felipesangas.englishdb.utilities.utilities;

/**
 * Created by felipesangas on 16/6/18.
 */

public class SQLiteConnectionHelper extends SQLiteOpenHelper {

    public SQLiteConnectionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(utilities.CREATE_ENTITIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS translations");
        onCreate(db);
    }
}
